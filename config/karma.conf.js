const karmaCommon = require('./karma.common');

module.exports = function (config) {
    karmaCommon.autoWatch = false;
    karmaCommon.singleRun = true;

    config.set(karmaCommon);
};
